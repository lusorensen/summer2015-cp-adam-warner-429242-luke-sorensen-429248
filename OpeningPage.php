<!DOCTYPE html>
<head>
<title>Log In</title>

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>

<script>
    

    
function logInForm(){
    $.ajax({url: "loginForm.html", success: function(result){
        $("#btn").html(result);
    }});
}

function registerForm(){
    $.ajax({url: "registerForm.html", success: function(result){
        $("#btn").html(result);
    }});
}

function restore() {
    $.ajax({url: "homeButtons.html", success: function(result){
        $("#btn").html(result);
    }});
}

</script>

<style>
    
html, body {
    height: 100%;
}

html {
    display: table;
    margin: auto;
}

body {
    font-family: "Lucida Console", Monaco, monospace;
    display: table-cell;
    vertical-align: middle;
    background-color: #FFEBFF;
}
	
h1 {
    font-size: 80px;
    font-weight: bold;
}

button {
  border-radius: 30px;
  font-family: "Lucida Console", Monaco, monospace;
  color: #ffffff;
  font-size: 31px;
  background: #3498db;
  padding: 15px 20px 15px 20px;
  text-decoration: none;
  border-width:0px
  
}

button:hover {
  background: #ffd4ff;
  text-decoration: none;
  
}

input[type=button] {
    
border-radius: 20px;
  font-family: "Lucida Console", Monaco, monospace;
  color: #ffffff;
  font-size: 20px;
  background: #3498db;
  padding: 10px 15px 10px 15px;
  text-decoration: none;
  border-width:0px
    
}

input[type=button]:hover {
  background: #ffd4ff;
  text-decoration: none; 
}

input[type=text] {
  font-family: "Lucida Console", Monaco, monospace;
}

input[type=text] {
  font-family: "Lucida Console", Monaco, monospace;
}



</style>


</head>
<body>

<h1 align=center> Share Space </h1>

<div id= "btn" align="center">
    
<button id="loginBtn" onclick= "logInForm()">Login</button>
&nbsp; &nbsp;
<button id="registerBtn" onclick= "registerForm()">Register</button>
    
</div>
        
        
</body>
</html>